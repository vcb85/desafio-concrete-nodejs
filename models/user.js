const mongoose = require('mongoose'),
      Schema   = mongoose.Schema;

module.exports = mongoose.model('User', new Schema({
    id: String,
    nome: String,
    email: String,
    token: String,
    senha: String,
    salt: String,
    data_criacao: Date,
    data_atualizacao: Date,
    ultimo_login: Date,
    telefones: Array
}));