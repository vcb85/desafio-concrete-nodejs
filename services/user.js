const User     = require('../models/user'),
      uuid     = require('uuid/v4'),
      Validate = require('../components/validate'),
      Crypt    = require('../components/crypt')
      config   = require('../config');

class UserService {
    save(data)
    {
        const validate       = new Validate,
              dataValidation = validate.user(data),
              instance       = this;

        if (dataValidation.error) {
            return dataValidation;
        }

        const findUserByEmail = this.findByEmail(data.email);

        return findUserByEmail.then(function (found) {
            if (found) {
                return {
                    "error": true,
                    "reason": "USER_FOUND_BY_MAIL"
                };
            }

            const crypt = new Crypt,
                  salt  = crypt.getSalt(config.salt_size),
                  user  = new User({
                      nome:      data.nome,
                      senha:     crypt.encrypt(data.senha, salt),
                      email:     data.email,
                      salt:      salt,
                      telefones: data.telefones
                  });


            user.data_criacao = new Date;
            user.id = uuid();

            return instance.authUser(user).save();
        });
    }
    authUser(user)
    {
        user.ultimo_login = new Date;
        user.token = uuid();
        return user;
    }
    signin(data)
    {
        const validate       = new Validate,
              dataValidation = validate.signin(data),
              instance       = this;

        if (dataValidation.error) {
            return dataValidation;
        }

        const findUserByEmail = this.findByEmail(data.email);

        return findUserByEmail.then(function (user) {
            if (!user) {
                return {
                    "error": true,
                    "reason": "USER_NOT_FOUND_BY_MAIL"
                };
            }
            const crypt = new Crypt;
            if (crypt.compare(data.senha, user.salt, user.senha)) {
                instance.authUser(user).save();
                return user;
            }
            return {
                "error": true,
                "reason": "WRONG_PASSWORD"
            };
        });
    }
    getById(id, token)
    {
        return User.findOne({token: token, id: id}).exec().then(function (user) {
            if (!user) {
                return {
                    "error": true,
                    "reason": "USER_NOT_FOUND_BY_ID"
                };
            }
            const now          = new Date,
                  thirtyMinAgo = new Date(now.getTime() - (30 * 60000));

            if (thirtyMinAgo.getTime() > user.ultimo_login.getTime()) {
                return {
                    "error": true,
                    "reason": "TOKEN_EXPIRED"
                };
            }
            return user;
        });
    }
    findByEmail(email)
    {
        return User.findOne({"email": email}).exec();
    }
}
module.exports = UserService;