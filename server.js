const express    = require('express'),
      app        = express(),
      morgan     = require('morgan'),
      bodyParser = require('body-parser'),
      config     = require('./config'),
      mongoose   = require('mongoose'),
      port       = process.env.PORT || 3000;

mongoose.connect(config.mongo_url);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(morgan('dev'));

app.use(require('./controllers'));

app.listen(port, function () {
  console.log('Listening on http://localhost:' + port)
});