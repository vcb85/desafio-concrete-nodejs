const express = require('express'),
      router = express.Router();

router.use('/user', require('./user'));

router.get('*', function(req, res) {

    res.status(404).send({mensagem: "NOT_FOUND"});
});

module.exports = router;