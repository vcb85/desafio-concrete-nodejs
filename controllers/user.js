const express     = require('express'),
      router      = express.Router(),
      UserService = require('../services/user.js');

router.post('/', function (req, res) {
    const userData    = req.body,
          userService = new UserService,
          result      = userService.save(userData);
    if (result.error) {
        return res.status(400).send({mensagem: result.reason});
    }

    result.then(function (user) {
        if (user.error) {
            const mensagem = user.reason == 'USER_FOUND_BY_MAIL'
                ? 'E-mail já existente' : user.reason;
            return res.status(400).send({mensagem: mensagem});
        }
        res.send(user);
    });
});

router.get('/signin', function (req, res) {
    const signinData  = req.query,
          userService = new UserService,
          result      = userService.signin(signinData);
    if (result.error) {
        return res.status(401).send({mensagem: "Usuário e/ou senha inválidos"});
    }

    result.then(function (user) {
        res.send(user);
    });
});

router.get('/:user_id', function (req, res) {
    const userId        = req.params.user_id,
          authorization = req.headers.authorization,
          userService   = new UserService;

    if (typeof(userId) == 'undefined' || typeof(authorization) == 'undefined') {
        return res.status(401).send({mensagem: "User_id ou header authorization ausentes"});
    }

    const token  = authorization.replace('Bearer ',''),
          result = userService.getById(userId, token);

    if (result.error) {
        return res.status(401).send({mensagem: "Não autorizado"});
    }

    result.then(function (user) {
        if (user.error) {
            const mensagem = user.reason == 'TOKEN_EXPIRED'
                                ? 'Sessão inválida'
                                : "Não autorizado";
            return res.status(401).send({mensagem: mensagem});
        }
        res.send(user);
    });
});
module.exports = router;