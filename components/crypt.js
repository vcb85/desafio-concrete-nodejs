var crypto = require('crypto');

class Crypt
{
    encrypt(password, salt)
    {
        const hash = crypto.createHmac('sha512', salt);

        hash.update(password);

        const encrypted = hash.digest('hex');
        return encrypted;
    }
    compare(plain, salt, encrypted)
    {
        return this.encrypt(plain, salt) == encrypted;
    }
    getSalt(length)
    {
        return crypto.randomBytes(Math.ceil(length / 2))
            .toString('hex')
            .slice(0, length);
    }
}

module.exports = Crypt;