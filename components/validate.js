const validator = require('validator');

class Validate
{
    user(data)
    {
        if (typeof(data.nome) == 'undefined' || validator.isEmpty(data.nome)) {
            return {error: true, reason: 'USER_NOME_INVALIDO'};
        }

        if (typeof(data.senha) == 'undefined' || validator.isEmpty(data.senha)) {
            return {error: true, reason: 'USER_SENHA_INVALIDA'};
        }

        if (typeof(data.email) == 'undefined' || !validator.isEmail(data.email)) {
            return {error: true, reason: 'USER_EMAIL_INVALIDO'};
        }

        if (typeof(data.telefones) != 'undefined' && data.telefones.lenght > 0) {
            for (let index in data.telefones) {

                let telefoneValido = this.telefone(data.telefones[index]);

                if (telefoneValido.error) {
                    return telefoneValido;
                }
            }
        }
        return {error: false};
    }
    telefone(data)
    {
        if (typeof(data.ddd) == 'undefined' || !validator.isNumeric(data.ddd)) {
            return {error: true, reason: 'TELEFONE_DDD_INVALIDO'};
        }
        if (typeof(data.numero) == 'undefined' || !validator.isNumeric(data.numero)) {
            return {error: true, reason: 'TELEFONE_NUMERO_INVALIDO'};
        }
        return {error: false};
    }
    signin(data)
    {
        if (typeof(data.senha) == 'undefined' || validator.isEmpty(data.senha)) {
            return {error: true, reason: 'SIGNIN_SENHA_INVALIDA'};
        }

        if (typeof(data.email) == 'undefined' || !validator.isEmail(data.email)) {
            return {error: true, reason: 'SIGNIN_EMAIL_INVALIDO'};
        }
        return {error: false};
    }
}

module.exports = Validate;