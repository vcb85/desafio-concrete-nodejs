const expect   = require('chai').expect,
      Validate = require('../../components/validate');

describe('Validate', function() {
      const validate = new Validate;
      describe('user', function() {
            it('should return error if nome is not sent', () => {
                const invalidData = {
                    "email": "string@string.com",
                    "senha": "senha",
                    "telefones": [ { "numero": "222256789", "ddd": "11" } ]
                };
                const expected = {error: true, reason: 'USER_NOME_INVALIDO'};
                const result   = validate.user(invalidData);

                expect(result.reason).to.equal(expected.reason);
            });

            it('should return error if senha is not sent', () => {
                const invalidData = {
                    "nome": "string",
                    "email": "string@string.com",
                    "telefones": [ { "numero": "222256789", "ddd": "11" } ]
                };
                const expected = {error: true, reason: 'USER_SENHA_INVALIDA'};
                const result   = validate.user(invalidData);

                expect(result.reason).to.equal(expected.reason);
            });

            it('should return error if email is not sent', () => {
                const invalidData = {
                    "nome": "string",
                    "senha": "senha",
                    "telefones": [ { "numero": "222256789", "ddd": "11" } ]
                };
                const expected = {error: true, reason: 'USER_EMAIL_INVALIDO'};
                const result   = validate.user(invalidData);

                expect(result.reason).to.equal(expected.reason);
            });
      });
      describe('telefone', function() {
            it('should return error if numero is not numeric', () => {
                const invalidData = {
                    "ddd": "11",
                    "numero": "asdasdasd"
                };
                const expected = {error: true, reason: 'TELEFONE_NUMERO_INVALIDO'};
                const result   = validate.telefone(invalidData);

                expect(result.reason).to.equal(expected.reason);
            });

            it('should return error if ddd is not numeric', () => {
                const invalidData = {
                    "numero": "123456789",
                    "ddd": "asdasdasd"
                };
                const expected = {error: true, reason: 'TELEFONE_DDD_INVALIDO'};
                const result   = validate.telefone(invalidData);

                expect(result.reason).to.equal(expected.reason);
            });
      });
      describe('signin', function() {

            it('should return error if senha is not sent', () => {
                const invalidData = {
                    "email": "string@string.com"
                };
                const expected = {error: true, reason: 'SIGNIN_SENHA_INVALIDA'};
                const result   = validate.signin(invalidData);

                expect(result.reason).to.equal(expected.reason);
            });

            it('should return error if email is not sent', () => {
                const invalidData = {
                    "senha": "senha"
                };
                const expected = {error: true, reason: 'SIGNIN_EMAIL_INVALIDO'};
                const result   = validate.signin(invalidData);

                expect(result.reason).to.equal(expected.reason);
            });
      });
});