const expect = require('chai').expect,
      Crypt  = require('../../components/crypt');

describe('Crypt', function() {
      const crypt = new Crypt;
      describe('encrypt', function() {
            it('should generate different hashes with different salts', () => {
                const password    = '1q2w3e4r',
                      salt        = '1234567890123456',
                      otherSalt   = '6543210987654321',
                      result      = crypt.encrypt(password, salt),
                      otherResult = crypt.encrypt(password, otherSalt);

                expect(result).to.not.equal(otherResult);
            });
      });
      describe('compare', function() {
            it('should match hashed passwords with same salt', () => {
                const password  = '1q2w3e4r',
                      salt      = '1234567890123456',
                      encrypted = crypt.encrypt(password, salt),
                      result    = crypt.compare(password, salt, encrypted);

                expect(result).to.equal(true);
            });
      });
});